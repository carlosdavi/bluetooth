package br.davi.bluetooth2;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import android.R.string;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.DataSetObserver;
import android.net.wifi.p2p.WifiP2pManager.ActionListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.View.OnLayoutChangeListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class NewDevices extends ListActivity {

	ListView listView;
	//String[] lista={"Davi","Camila","Fulano","Beltrano","Sicrano"} ;
	@Override
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		//setContentView(R.layout.new_devices);
		this.listView = (ListView) findViewById(R.id.paired_devices);
		Intent it = getIntent();
		//if(it != null){
			Bundle params = it.getExtras();
			String[] lstEstados;
			lstEstados = new String[] {"S�o Paulo", "Rio de Janeiro", "Minas Gerais", "Rio Grande do Sul",
				    "Santa Catarina", "Paran�", "Mato Grosso", "Amazonas"};

			if(params != null){
					this.setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,params.getStringArrayList("lista")));
				//this.listView.setAdapter(array);
				
				
				
				Toast.makeText(this, params.getStringArrayList("lista").get(0), Toast.LENGTH_LONG).show();
			}else{
				//Toast.makeText(this, "N�o tem nada no array", Toast.LENGTH_LONG).show();
			}
		}
	
	@Override
	 protected void onListItemClick(ListView l, View v, int position, long id) {
	  super.onListItemClick(l, v, position, id);
	 
	  //Pegar o item clicado
	  Object o = this.getListAdapter().getItem(position);
	  String itemSelecionado = o.toString();
	  
	  //Apresentar o item clicado
	  Toast.makeText(this, "Voc� clicou no estado : " + itemSelecionado, Toast.LENGTH_LONG).show();
	  //retorna a posi��o dele no array para a Activity principal
	  	Intent it = new Intent(this,MainActivity.class);
	  	Bundle params = new Bundle();
		String p = new Integer(position).toString();
	  	params.putString("retorno",p); //coloca a referencia dos devices
		it.putExtras(params); //manda para a outra activity a lista com os devices
		startActivity(it);
		
	}
			
	//}
	
}
